const express = require('express');
const app = express();
// Best app ever
app.get('/', (req, res) => res.send('CI/CD configuration example!'));

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));